/*
    Player for the DC Music Player (DMus)
*/

// Plays a given song
int play(const char* song);
