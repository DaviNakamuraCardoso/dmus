/*
*       Source code for the Diamond City Music Player (DMus)
*       Released Under the MIT License
*       (c) 2021 Davi Nakamura Cardoso
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <dmus/dimensions.h>
#include <jukebox/queue.h>
#include <tools/error.h>

int main(int argc, const char* argv[])
{
    printf("Hello, World!\n");
    return 0;
}
